#/bin/sh
docker-compose build --no-cache
docker-compose down
docker-compose up -d
docker image prune -af
docker volume prune -f