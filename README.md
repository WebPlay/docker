# Setup your own WebPlay instance

## Requirements
For run WebPlay as a docker container you need:
* docker
* docker-compose

installed on your server/computer

## Clone this repository
* `git clone https://gitea.com/WebPlay/docker.git webplay`

Change into folder
* `cd webplay`

Run docker-compose build
* `docker-compose build`

### Important
Open `docker-compose.yml` file and edit following lines:
```
volumes:
    - ./videos:/webplay/videos
    - ./music:/webplay/music
    - ./cache:/webplay/cache
    - ./uploads:/webplay/uploads
```
as example:
```
volumes:
    - /path/to/your/video/files:/webplay/videos
    - /path/to/your/audio/files:/webplay/music
    - /path/for/cache:/webplay/cache
    - /path/for/manual/uploads:/webplay/uploads
```

Optional you can change the client port (8081 by default):
```
ports:
    - 8081:80
```

Start your instance
* `docker-compose up` or `docker-compose up -d` for running WebPlay in background

Open your browser and navigate to WebPlay
* `http://YOURSERVERADRESS:8081`

That's it.

Have fun

## Support
Join our Matrix room: <a href="https://matrix.to/#/#WebPlay:matrix.anufrij.de">#WebPlay:matrix.anufrij.de</a>